import React, { useState } from "react";
import html2canvas from "html2canvas";
import { saveAs } from "file-saver";
import styled from "styled-components";
import useDimensions from "react-use-dimensions";

import Viz from "./Viz";
import data from "./flare-2.json";

const palette = {
  background: "rgb(255, 255, 255)",
  nodeText: "#35404C",
  packColors: [
    "#eeeeee",
    "rgba(0,0,0,0.05)",
    "rgba(0,0,0,0.05)",
    "rgba(0,0,0,0.05)",
    "rgba(0,0,0,0.05)",
    "pink"
  ],
  svgBackground: "#f05122",
  leaf: {
    fill: "#00c4dc",
    stroke: "#008D9F"
  }
};

const AppFlex = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100vh;
`;

const TitleBox = styled.div``;

const ButtonBox = styled.div`
  padding: 0.5rem 0 0 0;
`;

const VizBox = styled.div`
  flex: 1;
  align-self: stretch;
  display: flex;
  justify-content: center;
  margin: 0;
`;

function App() {
  const [selected, setSelected] = useState(null);
  const [ref, { width, height }] = useDimensions();

  const size = Math.min(width, height);

  return (
    <AppFlex>
      <TitleBox>
        <h1 style={{ margin: 0, padding: "0 0 2rem 0" }}>
          Selected: {selected || "N/A"}
        </h1>
      </TitleBox>
      <VizBox ref={ref}>
        {size ? (
          <Viz
            size={size}
            palette={palette}
            data={data}
            onChange={setSelected}
          />
        ) : null}
      </VizBox>
      <ButtonBox>
        <button
          style={{ margin: 0 }}
          onClick={() => {
            const el = document.querySelector("#encho");
            html2canvas(el).then(canvas => {
              canvas.toBlob(function(blob) {
                saveAs(blob, "Dashboard.png");
              });
            });
          }}
        >
          SAVE PNG (Buggy)
        </button>
      </ButtonBox>
    </AppFlex>
  );
}

export default App;
