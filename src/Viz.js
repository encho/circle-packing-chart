import React, {
  useRef,
  useEffect,
  memo
  //  useState
} from "react";
import styled from "styled-components";

import viz from "./zoomableCirclePacking";
import copyToClipboard from "./copyToClipboard";

const areEqual = (prevProps, nextProps) => {
  return JSON.stringify(prevProps.data) === JSON.stringify(nextProps.data);
};

// TODO do not render again as long as data, doeas not change
// TODO but: this may conflict with swapping theme!!
const Viz = memo(function({ size, palette, data, onChange }) {
  const svgRef = useRef();
  const width = size;
  const height = size;

  const setSelected = d => {
    onChange(d);
  };

  useEffect(() => {
    viz(data, svgRef.current, palette, { setSelected, copyToClipboard });
  }, []);

  const VizStyles = styled.div`
    box-sizing: border-box;
    margin: 0;
    display: inline-block;
    & {
      .node {
        cursor: pointer;
        transition: stroke 0.35s ease;
      }

      .node:hover {
        stroke-width: 2px;
      }
    }
  `;

  return (
    // {/* TODO set width and height and background color from the d3 code */}
    <VizStyles>
      <svg
        id="encho"
        style={{ backgroundColor: palette.svgBackground, display: "block" }}
        width={width}
        height={height}
        ref={svgRef}
      >
        <g />
      </svg>
    </VizStyles>
  );
}, areEqual);

export default Viz;
