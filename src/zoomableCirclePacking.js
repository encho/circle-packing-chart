// https://bl.ocks.org/fdlk/076469462d00ba39960f854df9acda56
import * as d3 from "d3";

// TODO pass in data as first argument!
const render = (data, svgEl, palette, { setSelected, copyToClipboard }) => {
  var svg = d3.select(svgEl),
    margin = 20,
    diameter = +svg.attr("width"),
    g = svg
      .append("g")
      .attr(
        "transform",
        "translate(" + diameter / 2 + "," + diameter / 2 + ")"
      );

  var color = d3
    .scaleOrdinal()
    .domain([0, 5]) // TODO get colors via type of node (city, advisor, client) instead of depth
    // .range(["hsl(152,80%,80%)", "hsl(228,30%,40%)"])
    // .range([palette.background, palette.primary])
    .range(palette.packColors);
  // .interpolate(d3.interpolateHcl);

  var pack = d3
    .pack()
    .size([diameter - margin, diameter - margin])
    .padding(2);

  //   console.log(pack);

  const root = d3
    .hierarchy(data)
    .sum(function(d) {
      return d.size;
    })
    .sort(function(a, b) {
      return b.value - a.value;
    });

  //   console.log("root:", root);

  var focus = root,
    nodes = pack(root).descendants(),
    view;

  //   console.log("nodes:", nodes);

  var circle = g
    .selectAll("circle")
    .data(nodes)
    .enter()
    .append("circle")
    // TODO: is class really necessary?
    .attr("class", function(d) {
      return d.parent
        ? d.children
          ? "node"
          : "node node--leaf"
        : "node node--root";
    })
    .style("fill", function(d) {
      return !d.children ? palette.leaf.fill : color(d.depth);
    })
    .style("stroke", function(d) {
      return !d.children ? palette.leaf.stroke : "#ccc";
    })
    .on("click", function(d) {
      setSelected(d.data.name);
      copyToClipboard(JSON.stringify(d.data));
      if (focus !== d) {
        zoom(d);
        d3.event.stopPropagation();
      }
    });

  var text = g
    .selectAll("text")
    .data(nodes)
    .enter()
    .append("text")
    // .attr("class", "label")
    .style("text-anchor", "middle")
    // .attr("fill", palette.nodeText)
    .style("fill", palette.nodeText)
    .style("fill-opacity", function(d) {
      return d.parent === root ? 1 : 0;
    })
    .style("display", function(d) {
      return d.parent === root ? "inline" : "none";
    })
    .text(function(d) {
      return d.data.name;
    });

  var node = g.selectAll("circle,text");

  svg.style("background", palette.background).on("click", function() {
    zoom(root);
  });

  zoomTo([root.x, root.y, root.r * 2 + margin]);

  function zoom(d) {
    var focus0 = focus;
    focus = d;

    var transition = d3
      .transition()
      .duration(d3.event.altKey ? 7500 : 750)
      .tween("zoom", function(d) {
        var i = d3.interpolateZoom(view, [
          focus.x,
          focus.y,
          focus.r * 2 + margin
        ]);
        return function(t) {
          zoomTo(i(t));
        };
      });

    var isTextVisible = d => {
      return d.parent === focus || (!focus.children && !d.children);
    };

    transition
      .selectAll("text")
      .filter(function(d) {
        return isTextVisible(d) || this.style.display === "inline";
      })
      .style("opacity", function(d) {
        return isTextVisible(d) ? 1 : 0;
      })
      .style("fill-opacity", function(d) {
        return isTextVisible(d) ? 1 : 0;
      })
      .on("start", function(d) {
        if (isTextVisible(d)) this.style.display = "inline";
      })
      .on("end", function(d) {
        if (!isTextVisible(d)) this.style.display = "none";
      });
  }

  function zoomTo(v) {
    var k = diameter / v[2];
    view = v;
    node.attr("transform", function(d) {
      return "translate(" + (d.x - v[0]) * k + "," + (d.y - v[1]) * k + ")";
    });
    circle.attr("r", function(d) {
      return d.r * k;
    });
  }
};

export default render;
